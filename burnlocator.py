# Helping Star Trek Discovery find the origin of the Burn
#
# Author: Heikki Orsila <heikki.orsila@@iki.fi>
#
# SPDX-License-Identifier: BSD-3-Clause
#
# Without loss of generality, coordinates in galaxy are located in the unit
# cube and velocity == 1 means the speed of light ;-)

import argparse
import pprint
import torch
import torch.nn as nn
import torch.optim as optim


class LossFunction(nn.Module):
    def __init__(self, n: int, v):
        super(LossFunction, self).__init__()
        assert n > 0
        assert torch.is_tensor(v)
        self._n = n
        self._v_squared = v * v
        self.position = nn.Parameter(torch.rand(3))
        self.t = nn.Parameter(torch.rand(1))
        self._loss = nn.MSELoss()
        self._zero = torch.tensor(0.0)

    def forward(self, positions, times):
        distances = torch.sum((positions - self.position)**2, dim=1)
        traveled = torch.sum(self._v_squared * ((times - self.t)**2), dim=1)
        # Add loss penalty for origin time that exceeds measured time (a
        # causality violation)
        violations = torch.abs((torch.min(times - self.t, self._zero)))
        penalty = torch.exp(2 * torch.max(violations)) - 1.0
        return self._loss(distances, traveled) + penalty


def _init_torch():
    torch.set_default_dtype(torch.float64)


class Burn:
    def __init__(self, num_points: int):
        self.num_points = num_points
        self.positions = torch.rand(num_points, 3)
        self.real_origin = torch.rand(3)
        self.real_time = torch.rand(1)
        self.v = torch.tensor(1.0)
        self.times = self._simulate_burn()

    def _simulate_burn(self):
        # Simulate the 'burn' from (real_origin, real_time) space-time
        # coordinates
        #
        # Calculate times for the burn to hit positions at velocity v.
        delta_pos = (self.positions - self.real_origin)
        distances = torch.sqrt(torch.sum(delta_pos**2, dim=1))
        propagation_times = distances / self.v
        times = propagation_times + self.real_time
        return times.reshape(self.num_points, 1)

    def __str__(self):
        d = {}
        for name in ('num_points', 'positions', 'real_origin', 'real_time',
                     'v', 'times'):
            d[name] = getattr(self, name)
        return pprint.pformat(d, indent=4)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--max-iters', type=int, default=65536)
    parser.add_argument(
        '--num-points', type=int, default=4,
        help=('Number of data points measured in space to find the burn. '
              'Need at least 4 data points for a unique solution. '
              'The current implementation does not always converge with 4 '
              'points. It converges more reliably with a value > 4.'))
    parser.add_argument('--verbose', action='store_true')
    args = parser.parse_args()
    if args.num_points < 4:
        print('The problem is ill-defined with less than 4 points. '
              'Keep searching!')

    _init_torch()

    burn = Burn(args.num_points)
    BURN_ROUND_LIMIT = 16
    burn_round_i = 0
    while burn_round_i < BURN_ROUND_LIMIT:
        solution = _burn_round(burn, args)
        if solution is None:
            print('No convergence at round', burn_round_i)
        else:
            position, t = solution
            print('solution distance to real origin:',
                  torch.norm(position - burn.real_origin))
            print('solution time minus origin time:', t - burn.real_time)
            print('solution position:', position,
                  'real origin:', burn.real_origin)
            print('solution time:', t,
                  'real time:', burn.real_time)
            if (torch.norm(position - burn.real_origin) < 0.001 and
                    torch.abs(t - burn.real_time) < 0.001):
                # The solution is within tolerance
                break
            print('Failed at round', burn_round_i, burn.times)
        burn_round_i += 1

    if burn_round_i >= BURN_ROUND_LIMIT:
        print('No luck. Problem that failed:\n', burn)
    else:
        print('Success. Found the burn at.')


def _burn_round(burn, args):
    loss_f = LossFunction(burn.num_points, burn.v)

    loss = loss_f(burn.positions, burn.times)
    old_loss = loss.item()
    eps = 1e-10

    optimizer = optim.SGD(loss_f.parameters(), lr=0.01, momentum=0.5)

    no_decrease = 0
    opt_iter = 0
    LIMIT = 5
    while (no_decrease < LIMIT and old_loss >= eps and
           opt_iter < args.max_iters):
        optimizer.zero_grad()
        loss = loss_f(burn.positions, burn.times)
        if args.verbose:
            print('opt round', opt_iter, 'loss', loss)
        if loss.item() >= old_loss:
            no_decrease += 1
        else:
            no_decrease = 0
        old_loss = loss.item()
        loss.backward()
        optimizer.step()
        opt_iter += 1

    print('Number of optimization iterations:', opt_iter)

    if no_decrease >= LIMIT or opt_iter >= args.max_iters:
        return None

    return loss_f.position, loss_f.t


if __name__ == '__main__':
    main()
