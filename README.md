# burnlocator

Example:
```
$ python3 burnlocator.py
Number of optimization iterations: 18867
solution distance to real origin: tensor(8.4840e-05, grad_fn=<NormBackward0>)
solution time minus origin time: tensor([5.5397e-05], grad_fn=<SubBackward0>)
solution position: Parameter containing:
tensor([0.6597, 0.7549, 0.8106], requires_grad=True) real origin: tensor([0.6598, 0.7548, 0.8107])
solution time: Parameter containing:
tensor([0.8674], requires_grad=True) real time: tensor([0.8674])
Success. Found the burn at.
```
